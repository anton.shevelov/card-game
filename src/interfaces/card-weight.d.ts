export interface CardWeight {
  value: string;
  weight: number;
}
