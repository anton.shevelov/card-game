import { Card } from 'interfaces';

export interface Player {
  id: number;
  name: string;
  hand: Card[];
  handWeight: number;
  handPairs?: number[];
  isWon?: boolean;
}
