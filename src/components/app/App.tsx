import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import BoardComponent from 'components/board/board';
import './App.scss';

export default () => (
  <Router>
    <Route path='/' exact component={BoardComponent} />
  </Router>
);
