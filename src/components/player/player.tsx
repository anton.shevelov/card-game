import React, { Component } from 'react';
import './player.scss';
import { observer } from 'mobx-react';
import { Card } from 'interfaces/card';
import { Player } from 'interfaces/player';
import { CardComponent } from 'components';
interface IProps {
  player: Player;
}

@observer
export default class PlayerComponent extends Component<IProps> {
  renderCard = (card: Card) => (
    <CardComponent
      key={card.id}
      card={card}
      className={
        this.props.player.isWon &&
        this.props.player.handPairs?.includes(card.value.weight)
          ? `pair${this.props.player.handPairs.indexOf(card.value.weight)}`
          : ''
      }
    />
  );
  render() {
    const { player } = this.props;
    return (
      <div className='player'>
        {player.name} cards
        <div className='cards-list p-1'>
          {player.hand.map(this.renderCard)}
        </div>
      </div>
    );
  }
}
