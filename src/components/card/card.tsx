import React, { Component } from 'react';
import './card.scss';
import { observer } from 'mobx-react';
import { Card } from 'interfaces/card';

interface IProps {
  card: Card;
  className?: string;
}

@observer
export default class CardComponent extends Component<IProps> {
  render() {
    const cardImg = `http://h3h.net/images/cards/${this.props.card.suit}_${this.props.card.value.value}.svg`;
    return (
      <img
        src={cardImg}
        className={`card ${this.props.className}`}
        alt='card'
      />
    );
  }
}
