import { observable, computed, action } from 'mobx';
import { Player } from 'interfaces/player';

export default class PlayersStorage {
  @observable private _players: Player[] = [];
  @observable private _winner: Player | null = null;

  private static _instance: PlayersStorage;

  static get instance(): PlayersStorage {
    if (!PlayersStorage._instance) {
      PlayersStorage._instance = new PlayersStorage();
    }
    return PlayersStorage._instance;
  }

  @computed get players(): Player[] {
    return this._players;
  }
  @computed get winner(): Player | null {
    return this._winner || null;
  }
  set winner(winner: Player | null) {
    this._winner = winner;
  }

  @action generatePlayers = () => {
    this._players = [];
    this._winner = null;
    this._players.push({
      id: 0,
      name: 'Player 1',
      hand: [],
      handWeight: 0,
    });
    this._players.push({
      id: 1,
      name: 'Player 2',
      hand: [],
      handWeight: 0,
    });
  };
}
