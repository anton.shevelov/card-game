import { Card } from 'interfaces/card';
import { SUITS, CARDS } from 'constants/card-settings';
import PlayersStorage from 'storage/players.storage';
import { Player } from 'interfaces/player';
import { maxBy } from 'lodash';
export default class CardsService {
  private static _instance: CardsService;

  static get instance(): CardsService {
    if (!CardsService._instance) {
      CardsService._instance = new CardsService();
    }
    return CardsService._instance;
  }

  constructor() {
    this.playGame();
  }
  generateCardDeck(): Card[] {
    const resultDeck: Card[] = [];
    for (let suitValue of SUITS) {
      for (let cardValue of CARDS) {
        const card = {
          suit: suitValue,
          id: resultDeck.length,
          value: cardValue,
        };
        resultDeck.push(card);
      }
    }
    return resultDeck;
  }

  deal(deck: Card[], players: any[], numCards: number) {
    players.forEach((player) => {
      while (player.hand.length !== numCards) {
        player.hand.push(
          deck.splice(Math.floor(Math.random() * deck.length), 1)[0]
        );
      }
    });
  }

  playGame(): void {
    PlayersStorage.instance.generatePlayers();
    this.deal(this.generateCardDeck(), PlayersStorage.instance.players, 5);
    this.findHandWeight(PlayersStorage.instance.players);
    this.findWinner(PlayersStorage.instance.players);
  }
  findHandWeight(players: Player[]) {
    players.forEach((player, index, players) => {
      const duplicates = player.hand
        .map((card) => card.value.weight)
        .filter(
          (elm, index, hand) => hand.indexOf(elm) !== hand.lastIndexOf(elm)
        );
      player.handWeight = duplicates.reduce((a, b) => a + b, 0);
      player.handPairs = this.countDuplicates(duplicates);
    });
  }
  countDuplicates(original: number[]): number[] {
    const uniqueItems = new Set();
    const duplicates = new Set();
    for (const value of original) {
      if (uniqueItems.has(value)) {
        duplicates.add(value);
        uniqueItems.delete(value);
      } else {
        uniqueItems.add(value);
      }
    }
    return Array.from(duplicates as any);
  }
  findWinner(players: Player[]): void {
    const playerWithHighestHand = maxBy(players, 'handWeight');
    if (playerWithHighestHand?.handWeight) {
      playerWithHighestHand.isWon = true;
      PlayersStorage.instance.winner = playerWithHighestHand;
    }
  }
}
